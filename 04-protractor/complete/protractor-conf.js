exports.config = {
  allScriptsTimeout: 11000,
  specs: [
    'protractor-test.js',
    'protractor-po-test.js',
  ],
  capabilities: {
    'browserName': 'chrome'
  },
  baseUrl: 'http://localhost:8000/04-protractor/complete',
  framework: 'jasmine2',
  jasmineNodeOpts: {
    defaultTimeoutInterval: 30000
  }
};
