'use strict';

function authInterceptor($injector, $q) {

  return {
    responseError: function(response) {
      return $injector.invoke(function(authModal, authService, $http) {

        function isRecoverable(response) {
          return response.status === 401 && !authService.isLoginUrl(response.config.url);
        }

        if (isRecoverable(response)) {
          return authModal.showLoginModal()
            .then(function() {
              return $http(response.config);
            });
        }

        return $q.reject(response);
      });
    }
  };
}

function tokenInterceptor($injector) {
  var AUTH_HEADER = 'X-Auth-Token';

  return {
    request: function(config) {

      var authService = $injector.get('authService');
      if (authService.isAuthenticated()) {
        config.headers = config.headers || {};
        config.headers[AUTH_HEADER] = authService.getToken();
      }

      return config;
    }
  };
}

function configInterceptors($httpProvider) {
  $httpProvider.interceptors.push('authInterceptor');
  $httpProvider.interceptors.push('tokenInterceptor');
}

function configRouter($routeProvider, $locationProvider) {

  $locationProvider.hashPrefix('');

  $routeProvider
    .when('/', {
      templateUrl: 'orderList.html',
      controller: 'OrderListController',
      controllerAs: 'list',
      resolve: {
        ordersData: function(Order) {
          return Order.query().$promise;
        }
      }
    })

    .when('/detail/:id', {
      templateUrl: 'orderDetail.html',
      controller: 'OrderDetailController',
      controllerAs: 'detail',
      resolve: {
        orderData: function(Order, $route) {
          var id = $route.current.params.id;

          return Order.get({'id': id}).$promise;
        }
      }
    })

    .when('/create', {
      templateUrl: 'orderCreate.html',
      controller: 'OrderCreateController',
      controllerAs: 'create',
      resolve: {
        isAuthorized: function(authService, authModal) {
          return authService.isAuthenticated() || authModal.showLoginModal();
        }
      }
    })

    .otherwise('/');
}

angular.module('authApp', ['ngRoute', 'ngMessages', 'ngResource', 'ui.bootstrap'])
  .constant('REST_URI', '//angular-cz-security-api.herokuapp.com')

  .factory("authInterceptor", authInterceptor)
  .factory("tokenInterceptor", tokenInterceptor)

  .run(function($rootScope, $location) {
    $rootScope.$on('login:loggedOut', function() {
      $location.path("/")
    });

    $rootScope.$on('$routeChangeError', function() {
      $location.path('/');
    });
  })

  .config(configInterceptors)
  .config(configRouter);
