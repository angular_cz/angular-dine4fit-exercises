function ucFirst(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

function Generator() {
  var wordLibrary = ['beef', 'chicken', 'pork', 'bacon', 'chuck', 'short loin', 'sirloin', 'shank', 'flank', 'sausage', 'pork belly', 'shoulder', 'cow', 'pig', 'ground round', 'hamburger', 'meatball', 'tenderloin', 'strip steak', 't-bone', 'ribeye', 'shankle', 'tongue', 'tail', 'pork chop', 'pastrami', 'corned beef', 'jerky', 'ham', 'fatback', 'ham hock', 'pancetta', 'pork loin', 'short ribs', 'spare ribs', 'beef ribs', 'drumstick', 'tri-tip', 'ball tip', 'venison', 'turkey', 'biltong', 'rump', 'jowl', 'salami', 'bresaola', 'meatloaf', 'brisket', 'boudin', 'andouille', 'capicola', 'swine', 'kielbasa', 'frankfurter', 'prosciutto', 'filet mignon', 'leberkas', 'turducken', 'doner', 'kevin', 'landjaeger', 'porchetta', 'burgdoggen'];

  var minWordCount = 30;
  var maxWordCount = 70;

  this.generateParagraph_ = function(startWithBaconIpsum) {

    var numberOfWords = Math.floor(Math.random() * (maxWordCount - minWordCount)) + minWordCount;

    var words = startWithBaconIpsum ? ['Bacon', 'ipsum'] : [];
    for (var i = 0; i < numberOfWords; i++) {
      var randomNumber = Math.floor(Math.random() * (wordLibrary.length - 1));
      var word = wordLibrary[randomNumber];
      words.push(word);
    }

    words[0] = ucFirst(words[0]);

    return words.join(' ') + '.';
  };

  this.getParagraphs = function(count) {
    count = count || 1;

    var paragraphs = [];

    for (var i = 0; i < count; i++) {
      paragraphs.push(this.generateParagraph_(i == 0));
    }

    return paragraphs;
  };
}

angular.module('ipsumService', [])
  .service('generator', Generator);
