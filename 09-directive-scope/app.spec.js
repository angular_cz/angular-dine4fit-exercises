describe('bacon-ipsum directive', function() {
  beforeEach(module('bacon', 'bacon.templates'));

  beforeEach(inject(function($rootScope, $compile, generator) {

    spyOn(generator, 'getParagraphs').and.callFake(
      function(numberOfParagraphs) {
        numberOfParagraphs = numberOfParagraphs || 1;

        var paragraphs = ['DATA_FROM_GENERATOR', 'SECOND_LINE', 'THIRD_LINE'];
        return paragraphs.slice(0, numberOfParagraphs);
      });

    this.$compile = $compile;
    this.$rootScope = $rootScope;

    this.$scope = this.$rootScope.$new();

    this.setTemplate = function(template) {
      this.element = angular.element(template);
    };

    this.render = function() {
      var element = this.$compile(this.element)(this.$scope);
      this.$scope.$digest();

      return element;
    };

    this.setTemplate('<bacon-ipsum></bacon-ipsum>');
  }));

  it('renders template (TODO 1.1)', function() {
    expect(this.render().html()).toMatch('<cite>Bacon ipsum</cite>');
  });

  it('renders paragraphs (TODO 1.2)', function() {
    expect(this.render().find('p').length).not.toBeFalsy();
  });

  it('renders paragraphs from generator (TODO 1.3)', function() {
    expect(this.render().find('p').html()).toMatch('DATA_FROM_GENERATOR');
  });

  describe('with multiple paragraphs', function() {

    it('renders number of paragraphs from generator (TODO 2.1)', function() {
      this.setTemplate('<bacon-ipsum paragraphs="2"></bacon-ipsum>');

      expect(this.render().find('p').length).toBe(2);
    });

    it('doesnt write data to parent scope (TODO 2.2)', function() {
      this.render();

      expect(this.$scope.data).not.toBeDefined();
    });
  });

  describe('with defined title', function() {

    beforeEach(function() {
      this.setTemplate('<bacon-ipsum title="{{titleVariable}}"></bacon-ipsum>');
    });

    it('has initial value from expression (TODO 3)', function(){
      this.$scope.titleVariable = 'TITLE_VARIABLE';
      expect(this.render().html()).toMatch('TITLE_VARIABLE');
    });

    it('can have value changed throught expression (TODO 3)', function(){
      var template = this.render();

      this.$scope.titleVariable = 'CHANGED_VARIABLE';
      this.$scope.$digest();

      expect(template.html()).toMatch('CHANGED_VARIABLE');
    });
  });

});
