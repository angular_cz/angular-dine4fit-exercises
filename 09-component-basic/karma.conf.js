var baseConfig = require('../base-karma.conf.js');
var courseWareConfig = require('../courseware-karma.conf.js');

module.exports = function(config) {
  baseConfig(config);
  courseWareConfig(config);

  config.set({
    basePath: '../'
  });

  config.plugins.push('karma-ng-html2js-preprocessor');

  config.files.push('09-component-basic/app.js');
  config.files.push('09-component-basic/app.spec.js');
  config.files.push('09-component-basic/service.js');
  config.files.push('09-component-basic/*.html');

  if (!config.preprocessors['**/*.html']) {
    config.preprocessors['**/*.html'] = [];
  }

  config.preprocessors['**/*.html'].push('ng-html2js');

  config.ngHtml2JsPreprocessor = {
    stripPrefix: '09-component-basic/',
    moduleName: 'bacon.templates'
  }
};
