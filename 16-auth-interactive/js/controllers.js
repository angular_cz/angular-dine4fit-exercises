'use strict';

function OrderListController(Order, ordersData) {
  this.orders = ordersData;

  this.statuses = {
    NEW: 'Nová',
    CANCELLED: 'Zrušená',
    PAID: 'Zaplacená',
    SENT: 'Odeslaná'
  };

  this.removeOrder = function(order) {
    order.$remove(function() {
      var index = this.orders.indexOf(order);
      this.orders.splice(index, 1);
    }.bind(this));
  };

  this.reloadOrder_ = function(order) {
    var index = this.orders.indexOf(order);

    Order.get({id: order.id}).$promise
      .then(function(originalOrder) {
        this.orders[index] = originalOrder;
      }.bind(this));
  };

  this.updateOrder = function(order) {

    order.$save()
      .catch(function() {
        this.reloadOrder_(order);
      }.bind(this));
  };
}

function OrderDetailController(orderData) {
  this.order = orderData;
}

function OrderCreateController(Order, $location) {
  this.order = new Order();

  this.save = function() {
    this.order.$save()
      .then(function() {
        $location.path("/orders");
      });
  };
}

function AuthController($uibModalInstance, authService) {

  this.authenticate_ = function(user, pass) {
    var closeModal = function() {
      $uibModalInstance.close();
    };

    var showBadLogin = function() {
      this.isBadLogin = true;
    }.bind(this);

    authService.login(user, pass)
      .then(closeModal)
      .catch(showBadLogin);
  };

  this.login = function() {
    this.authenticate_("operator", "password");
  };

  this.loginFail = function() {
    this.authenticate_("badUser", "badPassword");
  };

  this.cancel = function() {
    $modalInstance.dismiss('cancel');
  };
}

angular.module('authApp')
  .controller('OrderListController', OrderListController)
  .controller('OrderDetailController', OrderDetailController)
  .controller('OrderCreateController', OrderCreateController)
  .controller('AuthCtrl', AuthController);
